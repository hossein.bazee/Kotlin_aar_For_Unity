package com.cafebazaar.bazaarTask

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.farsitel.bazaar.ILoginCheckService
import com.unity3d.player.UnityPlayer

class LoginCheckActivity  {
    var service: ILoginCheckService? = null
    var connection: LoginCheckServiceConnection? = null
/*    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initService()
    }*/

    private fun initService(context: Activity) {
        Log.i(TAG, "initService()")
        connection = LoginCheckServiceConnection()
        val i = Intent(
            "com.farsitel.bazaar.service.LoginCheckService.BIND"
        )
        i.setPackage("com.farsitel.bazaar")
        val ret = context.bindService(i, connection!!, Context.BIND_AUTO_CREATE)
        Log.e(TAG, "initService() bound value: $ret")
    }

    private fun releaseService(context: Activity) {
        context.unbindService(connection!!)
        connection = null
        Log.d(TAG, "releaseService(): unbound.")
    }


    inner class LoginCheckServiceConnection : ServiceConnection {
        override fun onServiceConnected(
            name: ComponentName,
            boundService: IBinder
        ) {
            service = ILoginCheckService.Stub.asInterface(boundService)
            try {
                val isLoggedIn: Boolean = service!!.isLoggedIn()
                /*Toast.makeText(
                    this@LoginCheckActivity, "isLoggedIn:$isLoggedIn",
                    Toast.LENGTH_LONG
                ).show()*/
                print(isLoggedIn)
                UnityPlayer.UnitySendMessage(
                        // game object name
                        "LoginChecker",
                        // function name
                        "ShowLoginResult", // arguments

                        isLoggedIn.toString())


            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.e(
                TAG,
                "onServiceConnected(): Connected"
            )
        }

        override fun onServiceDisconnected(name: ComponentName) {
            service = null
            Log.e(
                TAG,
                "onServiceDisconnected(): Disconnected"
            )
        }
    }

    companion object {
        private const val TAG = "LoginCheck"
    }
}