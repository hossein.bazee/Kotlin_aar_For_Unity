package com.company.product;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayerActivity;


public abstract class OverrideUnityActivity extends UnityPlayerActivity
{
    public static OverrideUnityActivity instance = null;

    abstract protected void CloseUnity();
    abstract protected void FireBaseEvent(String str);
    abstract protected void ShowBtnRecord(int show);
    abstract protected void ShowBannerAd();
    abstract protected void FinishUnity();
    abstract protected void GetBtnRecordSize(int margin , int scale);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instance = null;
    }
}

