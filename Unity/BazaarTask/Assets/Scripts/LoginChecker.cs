﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginChecker : MonoBehaviour
{
    public Text txtLoginState;

    private AndroidJavaClass playerClass;
    private AndroidJavaObject activity;
    private AndroidJavaClass pluginClass;
    
    // Start is called before the first frame update
    void Start()
    {
         playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
         activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
         pluginClass = new AndroidJavaClass("com.cafebazaar.bazaarTask.LoginCheckActivity");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowLoginResult(string state)
    {
        print("reccive a massage");
        txtLoginState.text = state;
    }

    public void CheckLogin()
    {
     
        pluginClass.Call("initialize", new object[1] {activity});
    }

    public void OnApplicationQuit()
    {
        pluginClass.Call("releaseService", new object[1] {activity});
    }
    
    
}
